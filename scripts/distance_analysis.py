import os
import json
import numpy as np 
import pandas as pd
from scipy.spatial import distance
from sklearn.metrics.pairwise import cosine_similarity

#Getting config file for all category 
config_map_df = pd.read_csv('../notebook/mapping_config.csv')

#Reading dm_catalog into data frame 
category_df = pd.read_csv('../notebook/dm_catalog.csv')

# Fetching the parent category mapping deatils 
def get_parent_category_map(prod_with_category_df):
    """This function returns parent category map"""

    cat_list = ['STORAGE',
     'SERVERS',
     'HOSTING SERVICES',
     'CONTENT MANAGEMENT SYSTEMS',
     'IT INFRASTRUCTURE',
     'ARTIFICIAL INTELLIGENCE',
     'DEVELOPMENT',
     'ANALYTICS']

    try:
        cat_df = pd.DataFrame(cat_list,columns=['parent_cat'])
        merged_df = cat_df.merge( right=prod_with_category_df,how="left" ,left_on="parent_cat", right_on="PARENT_CATEGORY")

        parent_category_df = merged_df.groupby(by="parent_cat")['product_id'].count().apply(lambda category: 1 if category else 0).to_dict()
        parent_category_map = [(k, v) for k, v in parent_category_df.items()]

        return parent_category_map

    except Exception as ex:
        print(ex)

# Fetching the sub category mapping details
def get_sub_category_map(prod_with_category_df):
    """This function returns sub categories mapping"""

    sub_list= ['APPLICATION RELEASE ORCHESTRATION SOFTWARE',
     'TRANSACTIONAL EMAIL SOFTWARE',
     'EMAIL SOFTWARE',
     'AI PLATFORMS SOFTWARE',
     'EMAIL VERIFICATION SOFTWARE',
     'APPLICATION DEVELOPMENT SOFTWARE',
     'MACHINE LEARNING SOFTWARE',
     'DATA WAREHOUSE SOFTWARE',
     'SERVER VIRTUALIZATION SOFTWARE',
     'DATABASE SOFTWARE',
     'ENTERPRISE SEARCH SOFTWARE',
     'CONTAINERIZATION SOFTWARE',
     'STORAGE MANAGEMENT SOFTWARE',
     'LOAD BALANCING SOFTWARE',
     'APPLICATION PERFORMANCE MONITORING (APM) SOFTWARE',
     'CONTINUOUS DELIVERY SOFTWARE',
     'WEB ACCELERATOR SOFTWARE',
     'CLOUD PLATFORM AS A SERVICE (PAAS) SOFTWARE',
     'INFRASTRUCTURE AS A SERVICE (IAAS) PROVIDERS']

    try:
        sub_cat_df = pd.DataFrame(sub_list,columns=['sub_cat'])
        merged_df = sub_cat_df.merge( right=prod_with_category_df,how="left" ,left_on="sub_cat", right_on="SUB_CATEGORY")

        sub_category_df = merged_df.groupby(by="sub_cat")['product_id'].count().apply(lambda category: 1 if category else 0).to_dict()
        sub_category_map = [(k, v) for k, v in sub_category_df.items()]

        return sub_category_map

    except Exception as ex:
        raise Exception(f"Error while mapping sub categories")

#Fetching the cloud product
def get_product_map(prod_with_category_df):
    """This function checks organization empsize exclusion size"""
    prod_list = ['APACHE 2.2',
     'NGINX',
     'GOOGLE CLOUD PLATFORM',
     'AMAZON S3',
     'MICROSOFT AZURE BLOB STORAGE',
     'AMAZON AWS',
     'MICROSOFT AZURE',
     'APACHE 1.3',
     'APACHE 2.0',
     'APACHE 2.4']

    try:
        prod_list_df = pd.DataFrame(prod_list,columns=['products'])
        merged_df = prod_list_df.merge( right=prod_with_category_df,how="left" ,left_on="products", right_on="product")

        prod_category_df = merged_df.groupby(by="products")['product_id'].count().apply(lambda category: 1 if category else 0).to_dict()
        prod_category_map = [(k, v) for k, v in prod_category_df.items()]

        return prod_category_map

    except Exception as ex:
        raise Exception(f"Error while mapping prod_category")

# getting emp size mapping 
def get_empsize_map(org_empsize_range):
    """This function checks organization empsize exclusion size"""
    emp_size = org_empsize_range
    empsz_list_map = []
    empsize_list = ['10-to-50','50-to-200', '200-to-500']

    try:     
        #mapping of empsz_list_map when emp range is  '10-to-50'
        if emp_size == '10-to-50':

            vector_val = [1,0,0]
            empsz_list_map = list(zip(empsize_list,vector_val))
            return empsz_list_map                

        #mapping of empsz_list_map when emp range is  '50-to-200'
        if emp_size == '50-to-200':

            vector_val = [1,1,0]
            empsz_list_map = list(zip(empsize_list,vector_val))
            return empsz_list_map

        #mapping of empsz_list_map when emp range is  '200-to-500'
        if emp_size == '200-to-500':

            vector_val = [1,1,1]
            empsz_list_map = list(zip(empsize_list,vector_val))
            return empsz_list_map

        #mapping of empsz_list_map when emp range does not fall in any range
        if not empsz_list_map:

            vector_val = [0,0,0]
            empsz_list_map = list(zip(empsize_list,vector_val))
            return empsz_list_map

    except Exception as ex:
        raise Exception(f"Error while mapping empsize mapping")

def get_org_values(prod_intenisty_df):

    org_empsize_range = '0-to-0'

    #Merging category with with products
    prod_with_category_df = prod_intenisty_df.merge(right=category_df, left_on="product_id", right_on="DM_ID")

    #Getting parent category vector values distribution
    cat_list_map = get_parent_category_map(prod_with_category_df)

    #Getting sub category vector values distribution
    sub_list_map = get_sub_category_map(prod_with_category_df)

    #Getting product category vector values distribution
    prod_list_map = get_product_map(prod_with_category_df)

    #getting emp size range
    empsz_list_map = get_empsize_map(org_empsize_range)

    #Convert all the vector value distribution of org in same format as in config file vector distribution 
    try:
        total_map = cat_list_map +sub_list_map+prod_list_map+empsz_list_map
        total_map.sort()
        total_map_dict = dict(total_map)

        org = list(total_map_dict.values())
        org_value = np.array(org)

    except Exception as ex:
        raise Exception(f"Error while calculating vector value of org ")

    return org_value,total_map_dict


# Findng the cosine similarity for all the category for  the org
spend_cluster = ['10k-25k',
 '25k-50k',
 '50k-100k',
 '100k-150k',
 '200k-250k',
 '250k-500k',
 '500k+']

try:
    cos_dist_dict = {}
    eclud_dist_dict = {}
    hamming_dist_dict = {}
    jaccard_dist_dict = {}
    kulsinski_dist_dict = {}


    # reading tg_org with high intensity 


    for org_name in os.listdir('../org_data'):
        cos_dist_dict = {}
        eclud_dist_dict = {}
        hamming_dist_dict = {}
        jaccard_dist_dict = {}
        kulsinski_dist_dict = {}


        for clust in spend_cluster:
            file_location = '../org_data'+'/'+org_name
            prod_intenisty_df = pd.read_csv(file_location)
            org_value,total_map_dict = get_org_values(prod_intenisty_df)

            clust_var_data = config_map_df[config_map_df['spend']==clust][list(total_map_dict.keys())].values 

            #cosine distance
            cos_dist_dict[clust]= 1-distance.cosine(clust_var_data,org_value)

            #ecludian distance
            eclud_dist_dict[clust] = distance.euclidean(clust_var_data,org_value)

            #Hamming  distance
            hamming_dist_dict[clust] = distance.hamming(clust_var_data,org_value)

            # Jaccard Index  distance
            jaccard_dist_dict[clust] = distance.jaccard(clust_var_data,org_value)

            # kulsinski Index  distance
            kulsinski_dist_dict[clust] = distance.kulsinski(clust_var_data,org_value)

#            output = [clust ,org_name , cos_dist_dict , eclud_dist_dict , hamming_dist_dict , jaccard_dist_dict , kulsinski_dist_dict]
        print(org_name)
        print("cosine_similarity  : {}".format(max(cos_dist_dict, key=cos_dist_dict.get)))
        print("ecludian distance  : {}".format(min(eclud_dist_dict , key=eclud_dist_dict.get)))
        print("Hamming distance   : {}".format(min(hamming_dist_dict, key=hamming_dist_dict.get)))
        print("Jaccard distance   : {}".format(min(jaccard_dist_dict, key=jaccard_dist_dict.get)))
        print("kulsinski distance : {}".format(min(kulsinski_dist_dict, key=kulsinski_dist_dict.get)))




except Exception as ex:
    print(ex)
    raise Exception(f"Error while calculating cloud_spending")
